# Wildlife Icon Font

## Projet

Police de caractère dédiée à la faune sauvage.

## Authors

Projet communautaire 


## Outils	

Edition des icones par Inkscape ou Illustrator

 * [Avec inkscape (alternative libre à illustrator)](https://fr.flossmanuals.net/initiation-inkscape/creer-sa-police-de-caractere/)
 * [Avec Illustrator](https://www.alsacreations.com/tuto/lire/1547-police-font-icone-vectorielle-webdesign.html)

Plus d'informations ici:

https://www.webdesignerdepot.com/2012/01/how-to-make-your-own-icon-webfont/

Allez sur le fichier demo/index.html pour plus d'instructions
